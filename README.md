# domin-interview-task

This is a simple data handler application built with FastAPI for the backend and ReactJS for the frontend.

## Installation

1. **Clone the repository**:

   ```bash
   git clone https://github.com/brynclark1/domin-interview-task.git

2. **Install pip requirements**:

   ```bash
   pip install -r requirements.txt

3. **Run the API**:

   ```bash
   uvicorn app:app --reload
   

4. **Install Axios for ReactJS and Run the Web App**:

   ```bash
   cd frontend
   npm install axios
   npm start


You can view the real-time data and make filtering queries at http://localhost:3000/data


